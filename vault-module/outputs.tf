output "bucket_name_url" {
  value = google_storage_bucket.vault_storage_backend.self_link
}

output "bucket_name_id" {
  value = google_storage_bucket.vault_storage_backend.id
}

output "gcs_bucket_name" {
  value = var.gcs_bucket_name
}

output "gcp_project_id" {
  value = var.gcp_project_id
}

output "service_account_key" {
  value = google_service_account_key.vault.private_key
}

output "key_ring" {
  value = google_kms_key_ring.vault_keyring.name
}


output "crypto_key" {
  value = google_kms_crypto_key.vault_crypto_key.name
}

