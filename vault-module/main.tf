# ---------------------------------------------------------------------------------------------------------------------
# THESE TEMPLATES REQUIRE TERRAFORM VERSION 0.12.0 AND ABOVE
# This module has been updated with 0.12 syntax, which means the example is no longer
# compatible with any versions below 0.12.
# ---------------------------------------------------------------------------------------------------------------------

terraform {
  required_version = ">= 0.12"
}

# ---------------------------------------------------------------------------------------------------------------------
# CREATES A SERVICE ACCOUNT TO OPERATE THE VAULT CLUSTER
# The default project service account will be used if create_service_account
# is set to false and no service_account_email is provided.
# ---------------------------------------------------------------------------------------------------------------------

resource "google_service_account" "vault_cluster_admin" {
  account_id   = "vault-admin-sa"
  display_name = "Vault Server Admin"
  project      = var.gcp_project_id
}

# Create a service account key
resource "google_service_account_key" "vault" {
  service_account_id = google_service_account.vault_cluster_admin.name
}

# ---------------------------------------------------------------------------------------------------------------------
# CREATE A GOOGLE STORAGE BUCKET TO USE AS A VAULT STORAGE BACKEND
# ---------------------------------------------------------------------------------------------------------------------

resource "google_storage_bucket" "vault_storage_backend" {
  name          = var.gcs_bucket_name
  location      = var.gcs_bucket_location
  storage_class = var.gcs_bucket_storage_class
  project       = var.gcp_project_id
}

# ACLs are now deprecated as a way to secure a GCS Bucket (https://goo.gl/PgDCYb0), however the Terraform Google Provider
# does not yet expose a way to attach an IAM Policy to a Google Bucket so we resort to using the Bucket ACL in case users
# of this module wish to limit Bucket permissions via Terraform.
resource "google_storage_bucket_acl" "vault_storage_backend" {
  bucket         = google_storage_bucket.vault_storage_backend.name
  predefined_acl = var.gcs_bucket_predefined_acl
}


# Allows a provided service account to create and read objects from the storage
resource "google_storage_bucket_iam_binding" "vault_cluster_admin_service_acc_binding" {
  bucket = google_storage_bucket.vault_storage_backend.name
  role   = "roles/storage.objectAdmin"

  members = [
    "serviceAccount:${google_service_account.vault_cluster_admin.email}",
  ]

  depends_on = [
    google_storage_bucket.vault_storage_backend,
    google_storage_bucket_acl.vault_storage_backend,
  ]
}

# ---------------------------------------------------------------------------------------------------------------------
# CREATE A KMS KEY SO VAULT CAN USE AUTO UNSEAL
# ---------------------------------------------------------------------------------------------------------------------
resource "google_kms_key_ring" "vault_keyring" {
  name     = "vault-${var.kms_suffix}"
  location = "global"
  project     = var.gcp_project_id
 
}

resource "google_kms_crypto_key" "vault_crypto_key" {
   name            = "vault-${var.kms_suffix}"
   key_ring        = google_kms_key_ring.vault_keyring.self_link
   rotation_period = "100000s"


}

resource "google_kms_key_ring_iam_binding" "vault_kmskey_service_acc_binding" {
  key_ring_id = google_kms_key_ring.vault_keyring.self_link
  role = "roles/cloudkms.admin"

  members = [
    "serviceAccount:${google_service_account.vault_cluster_admin.email}",
  ]
}

resource "google_kms_crypto_key_iam_binding" "vault_crypto_key_binding" {
  crypto_key_id = google_kms_crypto_key.vault_crypto_key.id
  role = "roles/cloudkms.cryptoKeyEncrypterDecrypter"

  members = [
    "serviceAccount:${google_service_account.vault_cluster_admin.email}",
  ]

}
