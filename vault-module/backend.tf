terraform {
  backend "remote" {
    hostname = "app.terraform.io"
    organization = "techturbid"

    workspaces {
      name = "techturbidtools_vault"
    }
  }
}