# ---------------------------------------------------------------------------------------------------------------------
# REQUIRED PARAMETERS
# You must provide a value for each of these parameters.
# ---------------------------------------------------------------------------------------------------------------------

variable "service_account_key" {
  description = "Service account key from google"
  type        = string
}


## Change this for yout own application default credentials path
variable "service_account_key_path" {
  description = "The path of json credentials file"
  type        = string
  default     = "~/.google_credentials.json"
  
}

variable "gcp_region" {
  description = "All GCP resources will be launched in this Region."
  type        = string
}

variable "gcp_project_id" {
  description = "The ID of the GCP project to deploy the vault cluster to."
  type        = string
}

variable "gcs_bucket_name" {
  description = "The name of the Google Storage Bucket where Vault secrets will be stored."
  type        = string
}

variable "gcs_bucket_location" {
  description = "The location of the Google Storage Bucket where Vault secrets will be stored. For details, see https://goo.gl/hk63jH."
  type        = string
  default     = "us-east1"
}

variable "gcs_bucket_storage_class" {
  description = "The Storage Class of the Google Storage Bucket where Vault secrets will be stored. Must be one of MULTI_REGIONAL, REGIONAL, NEARLINE, or COLDLINE. For details, see https://goo.gl/hk63jH."
  type        = string
}

variable "gcs_bucket_predefined_acl" {
  description = "The canned GCS Access Control List (ACL) to apply to the GCS Bucket. For a full list of Predefined ACLs, see https://cloud.google.com/storage/docs/access-control/lists."
  type        = string
}

variable "kms_suffix" {
  description = "A kms suffix to differ among others"
}
