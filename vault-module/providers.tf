provider "google" {
  credentials = var.service_account_key == "" ? "${file("${var.service_account_key_path}")}" : var.service_account_key
  project     = var.gcp_project_id
}