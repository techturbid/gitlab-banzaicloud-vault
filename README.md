# Techturbid Vault

This directory contains a terraform module and 3 public charts (Banzai cloud) to deploy a full Vault integration with Kubernetes

1. Terraform module
* Vault google service account
* Vault google storage bucket
* Vault google key ring
* Vault google crypto key

2. Vault operator
* Banzai cloud Vault operator helm chart with some custom resources

3. Vault
* Banzai cloud Vault helm chart with additionals configurer, unsealer and prometheus containers.

4. Vault Secrets Webhook
* Banzai cloud Vault Mutation admission webhook helm chart.

5. Gitlab CI
* A small gitlab pipeline to install the whole feature.


## Gitlab CI
To run this pipeline, you need to check somethings:

1. If there is a runner installed in your kubernetes cluster.
2. If the runner has kubernetes and tooling as tags.
3. Since we run helm, We need to give cluster-admin permission to the default namespace service account that the runner is running.
`kubectl create clusterrolebinding cicd-admin --clusterrole=cluster-admin --serviceaccount=cicd:default`
4. You need to create a [TERRAFORM_TOKEN](https://app.terraform.io/app/settings/tokens) and [TERRAFORM_SA_CREDENTIALS](https://console.cloud.google.com/apis/credentials) variables.



## How to install

1. Setup terraform backend in `vault-module/backend.tf`
2. Change variables in vault-module/vault.tfvars

3. Run this commands
```
git clone https://gitlab.com/techturbid/vault.git
cd vault/vault-module
terraform apply -auto-approve -var-file=vault.tfvars
echo "$(terraform output service_account_key)" >> google_key
echo "$(terraform output gcp_project_id)" >> project_id
echo "$(terraform output gcs_bucket_name)" >> bucket_name
echo "$(terraform output key_ring)" >> key_ring
echo "$(terraform output crypto_key)" >> crypto_key
cd ../
export K8S_NAMESPACE=vault
helm upgrade --install vault-operator --namespace ${K8S_NAMESPACE} vault-operator/
cat <<EOF> values-vault.yaml
      google:
        credentials: "$(cat vault-module/outputs/google_key)"
      vault:
        config:
          storage:
            gcs:
              bucket: "$(cat vault-module/outputs/bucket_name)"
          seal:
            gcpckms:
              project: "$(cat vault-module/outputs/project_id)"
              key_ring: "$(cat vault-module/outputs/key_ring)"
              crypto_key: "$(cat vault-module/outputs/crypto_key)"
      EOF
helm upgrade --install vault --namespace ${K8S_NAMESPACE} vault/ -f values-vault.yaml
helm upgrade --install vault-webhook --namespace ${K8S_NAMESPACE} vault-secrets-webhook/

```
